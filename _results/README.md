## Results from ‘Swashing-Machine’ workshop, may 2021

![](img/SwashingMachine-interface2.gif)
(…)

## ————————————————————

![](img/SwashingMachine-photos-01.png)
![](img/SwashingMachine-photos-02.png)
![](img/SwashingMachine-glyphs.gif)
![](img/SwashingMachine-photos-03.png)
![](img/SwashingMachine-photos-04.png)
![](img/SwashingMachine-photos-05.png)
![](img/SwashingMachine-interface.gif)
![](img/SwashingMachine-photos-06.png)
![](img/SwashingMachine-photos-07.png)
![](img/SwashingMachine-photos-08.png)
