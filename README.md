**S W A S H I N G — M A C H I N E**<br>
: :  Bonjour Monde<br>
: :  La Martinière Diderot, Lyon<br>
: :  3-5 mai 2021<br>


————————————<br>
**S U J E T**

De l'[Alphabet Lapidaire Monstre](https://www.flickr.com/photos/letterformarchive/27930986149/sizes/h/) au [Nitto Mostro](https://boldmonday.com/typeface/nitti-mostro/), les caractères ornés sont les *freaks* du design typographique. Ils offrent pourtant des terrains de jeu très fertiles, grâce à leurs formes débridées et l'aspect souvent combinatoire amené par le système de strates.<br>
L'objectif de cet atelier sera d'explorer à la fois des formes typographiques modulaires et leur ornement, le tout selon un structure de base commune et de calques séparés qui nous permettront de fusionner/confronter l'ensemble des créations dans un seul caractère aux visages multiples.

————————————<br>
**M É T H O D O L O G I E**

Le travail se fera en binôme (Graphisme/Numérique). Chaque binôme sera chargé de proposer un ou plusieurs systèmes typographiques, en prenant comme base le caractère typographique [Monsta](https://gitlab.com/bonjour-monde/projects/swashing-machine/-/blob/master/fonts/Monsta-Basic.glyphs) (composé uniquement de morceaux de lettres, séparés en calques).
<br>Un “système typographique” se composera de deux parties :<br>
• un travail d'ornement, contenu dans les éléments “pleins” du caractère de base. Ces ornements devront être faits de tracés filaires, destinés à être exportés au format SVG et dessinés au plotter. Leur méthode de conception est absolument libre (dessin vectoriel, scripts génératifs…), de même que leur forme (illstrative/abstraite, simple/complexe…)<br>
• un travail sur la forme-même des lettres, selon un principe de modules à manipuler (exemple : les empattements du caractère, la barre horizontale du H…). La modification de ces quelques éléments viendra se répéter sur l'ensemble des lettres et la moindre altération donnera au caractère entier une nouvelle identité.



————————————<br>
**C A L E N D R I E R**

Lundi 3 mai
<br>— MATIN :
<br>- présentation de Bonjour Monde 
<br>- exercices de dessin / pratique du motif en vectoriel
<br>— APRÈS-MIDI :
<br>- présentation du plotter 
<br>- introduction du sujet

Mardi 4 mai
<br>— MATIN :
<br>- Travail graphique en autonomie
<br>— APRÈS-MIDI :
<br>- impressions et début d’accrochage pour regard collectif 
<br>- poursuite des recherches graphiques
<br>- rendez-vous en groupes


Mercredi 5 mai
<br>— MATIN :
<br>- Travail graphique en autonomie
<br>— APRÈS-MIDI :
<br>- impression "aléatoire" de lettres + motifs (imprimante lase + plotter) 
<br>- restitution et accrochage bilan

————————————
<br>**O U T I L S**
<br>• [Inkscape](https://inkscape.org/release/inkscape-1.0.2/)
<br>• [Processing](https://processing.org/download/)
<br>• [Glyphs (payant, Mac-only, version d'essai de 30 jours)](https://glyphsapp.com/buy)
<br>• [FontLab (payant, Mac & PC, version d'essai de 30 jours)](https://www.fontlab.com/font-editor/fontlab/#download)
<br>• [FontForge (libre, Mac & PC & Linux)](https://fontforge.org/en-US/downloads/)
<br>• [BirdFont (gratuit si non-commercial, Mac & PC & Linux)](https://birdfont.org/#download)

————————————<br>
**L I E N S**<br>
• <a href="https://twitter.com/Jean_no/status/1345881516161839104">Les programmes processing de Jean-Noël Lafargue en un tweet </a><br>
• <a href="https://fr.flossmanuals.net/fontes-libres/dessiner-avec-les-courbes-spiro-dans-inkscape/">Un tuto Inkscape<br></a>
• <a href="https://inkscape.org/fr/doc/tutorials/advanced/tutorial-advanced.html">Un autre tuto Inkscape<br></a>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Metaflop](https://www.metaflop.com/modulator)<br>
• [Références de lettres ornées](https://www.are.na/lucas-descroix/swashing-machine)



————————————<br>
**R É F É R E N C E S**
<br>• <a href="http://www.mmparis.fr/texts/mm_cassagnau.html">Un texte sur l'ornement dans le travail de M&M</a>
<br>
• <a href="http://www.conjonction.org/eon1#2">Un texte de Lovecraft sur les modernes qui s'aiment pas les ornements et c'est nul</a><br>
• Adolf Loos, <i>Ornement et crime</i>, Rivages, Paris, 2015<br>
• Walter Benjamin, <i>Sur le haschich</i>, Christian Bourgois Editeur, Paris, 2011<br>
————————————<br>
**Art Generatif**
https://en.wikipedia.org/wiki/Turing_pattern
https://www.reddit.com/r/generative/
https://en.wikipedia.org/wiki/Reaction%E2%80%93diffusion_system
https://en.wikipedia.org/wiki/Cellular_automaton
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
https://natureofcode.com/book/

